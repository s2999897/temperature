package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {
    @Test
    public void testDegrees5 () throws Exception {
        Converter converter = new Converter();
        double price = converter.getFahrenheit("5");
        Assertions.assertEquals(41.0, price, 0.0, "5 C in faren");
    }

}

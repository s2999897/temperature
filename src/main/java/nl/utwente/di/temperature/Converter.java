package nl.utwente.di.temperature;

public class Converter {
    public double getFahrenheit(String temp) {
        return (((Double.valueOf(temp)*9)/5.0) + 32);
    }
}
